import React from 'react';
import { Link } from 'react-router-dom';
import './Rating.scss';
import ratingIcon from './../../img/rating-icon.svg';
import logo from './../../img/logo_full.svg';
import axios from 'axios';
import { Preloader } from '../../components/UI/Preloader/Preloader';
import { Helmet } from 'react-helmet';

class Rating extends React.Component {
	state = {
		pointsList: [],
		pointsSortedList: [],
		isLoading: true,
		sort: [
			{
				name: 'По рейтингу',
				fieldName: 'mark_avg',
			},
			{
				name: 'По дате',
				fieldName: 'id',
			}
		],
		currentSort: 0
	}
	getRating = async () => {
		try {
			const response = await axios.get('https://shaurma-kh.space/functions/api/get_rating.php');
			this.sortArray(response.data, this.state.sort[this.state.currentSort].fieldName);
			
			this.setState({
				isLoading: false,
				pointsList: response.data
			});
		} catch (e) {
			alert('Возникла ошибка')
			console.log(e)
		}
	}
	sortArray = (array, param) => {
		const sortedResult = array.sort((a, b) => (b[param] - a[param]));
		let filteredResult = param === 'mark_avg'
			? sortedResult.filter(elem => elem.mark_qn >3)
			: sortedResult
		this.setState({
			pointsSortedList: filteredResult
		});
	}
	changeSort = (index) => {
		this.setState({
			currentSort: index
		});
		this.sortArray(this.state.pointsList, this.state.sort[index].fieldName);
	}

	componentDidMount() {
		this.getRating()
	}
	render() {
		return (
			<div className="app-section rating-section">
				<Helmet>
					<title>Рейтин самой вкусной шаурмы в Харькове -  Мы знаем где лучшая шаурма в городе</title>
					<meta name="description"
						content="" />
				</Helmet>
				<div className="section-inner">
					<div className="section-title">
						Рейтинг
					</div>
					<div className="rating-table">
						<div className="rating-order">
							<div className="rating-order-title">Сортировка:</div>
							<div className="rating-order-list">
								{this.state.sort.map((elem, index) => (
									index === this.state.currentSort
										? <div className="rating-order-item active" key={index}>{elem.name}</div>
										: <button className="rating-order-item" key={index} onClick={() => this.changeSort(index)}>{elem.name}</button>
								)
								)}
							</div>
						</div>
						<div className="rating-title">
							<div className="rating-title-row">
								<div className="rating-img">Фото</div>
								<div className="rating-name">Название</div>
								<div className="rating-price">Цена</div>
								<div className="rating-qn">Кол-во голосов</div>
								<div className="rating-mark">Оценка</div>
								<div className="rating-place">&nbsp;</div>
							</div>
						</div>
						<div className="rating-content">
							{this.state.isLoading
								? <Preloader />
								: this.state.pointsSortedList.map((elem, key) => (
									<div className="rating-row" key={key}>
										<div className="rating-img">
											<img src={
												elem.img !== '/img/logo.jpg'
													? elem.img
													: logo
											} alt={elem.name}
											/>
										</div>
										<div className="rating-name">{elem.name}</div>
										<div className="rating-price">{elem.price}</div>
										<div className="rating-qn">{elem.mark_qn}</div>
										<div className="rating-mark">{elem.mark_avg}</div>
										<div className="rating-place">
											<Link to={'/point/' + elem.id} >
												<img src={ratingIcon} alt="" />
											</Link>
										</div>
									</div>
								)
								)
							}
						</div>
					</div>
				</div>
			</div>
		)
	}
}
export default Rating;