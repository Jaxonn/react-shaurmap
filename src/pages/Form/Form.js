import React from 'react';
import './Form.scss';

import GoogleMapReact from 'google-map-react'
import FormMapPoint from '../../components/FormMapPoint/FormMapPoint'
import OwnPositionPoint from '../../components/MapMain/OwnPositionPoint'
import { Preloader } from '../../components/UI/Preloader/Preloader'
import axios from 'axios'
import { Helmet } from 'react-helmet'

import FormPopup from '../../components/FormPopup/FormPopup'

class Form extends React.Component {
	state = {
		pointData: {
			coordinates: {
				lat: null,
				lng: null
			},
			name: "",
			isChecked: false,
			size: null,
			price: "",
			mark: null,
			description: "",
			image: null,
			previewImage: null
		},
		mapSettings: {
			center: {
				lat: 50.00000,
				lng: 36.22917
			},
			zoom: 11
		},
		isPopupShow: false,
		isLoading: false,
		errors: [],
	}
	componentDidMount() {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(position => {
				const pos = {
					lat: position.coords.latitude,
					lng: position.coords.longitude
				}
				this.setState({
					mapSettings: {
						center: pos,
						zoom: 16
					}
				})
			})
		}
	}
	resetFormFields = () => {
		this.setState({
			pointData: {
				coordinates: {
					lat: null,
					lng: null
				},
				name: "",
				isChecked: false,
				size: null,
				price: "",
				mark: null,
				description: "",
				image: null,
				previewImage: null
			}
		})
	}
	showPopupHandler = () => {
		this.setState({
			isPopupShow: true
		})
	}
	hidePopupHandler = () => {
		this.setState({
			isPopupShow: false
		})
	}
	handleMapClick = ({ lat, lng }) => {
		this.setState({
			pointData: {
				...this.state.pointData,
				coordinates: {
					lat,
					lng
				}
			}
		})
	}
	changePointStatus = () => {
		this.setState({
			pointData: {
				...this.state.pointData,
				isChecked: !this.state.pointData.isChecked
			}
		})
	}
	changeInputFileHandler = event => {
		const file = event.target.files[0];
		if (!file.type.match('image.*')) {
			alert('Неверный формат файла')
			return
		}
		const canvas = document.createElement("canvas");
		const ctx = canvas.getContext("2d");
		const maxW = 800;
		const maxH = 600;

		const fileName = event.target.files[0].name
		const img = new Image()
		img.onload = () => {
			const iw = img.width
			const ih = img.height
			const scale = Math.min((maxW / iw), (maxH / ih))
			const iwScaled = iw * scale
			const ihScaled = ih * scale
			canvas.width = iwScaled
			canvas.height = ihScaled
			ctx.drawImage(img, 0, 0, iwScaled, ihScaled)
			const output = canvas.toDataURL("image/jpeg", 0.5)
			const parts = output.split(',')
			const type = parts[0]
			const base64Data = parts[1]
			const blobImage = this.b64toBlob(base64Data, type)
			this.setState({
				pointData: {
					...this.state.pointData,
					previewImage: output,
					image: {
						name: fileName,
						content: blobImage
					}
				}
			})
		}
		img.src = URL.createObjectURL(event.target.files[0]);
	}
	removeLoadedFile = () => {
		this.setState({
			pointData: {
				...this.state.pointData,
				image: null,
				previewImage: null
			}
		})
	}

	changeInputHandler = event => {
		this.setState({
			pointData: {
				...this.state.pointData,
				[event.target.name]: event.target.value
			}
		})
	}
	addNewPoint = async event => {
		event.preventDefault()

		const errors = [];
		if (!this.state.pointData.name) {
			errors.push('Название');
		}
		if (!this.state.pointData.coordinates.lat || !this.state.pointData.coordinates.lat) {
			errors.push('Точка на карте');
		}

		if (!errors.length) {
			this.setState({
				isLoading: true
			})
			const formData = new FormData()

			formData.set('name', this.state.pointData.name)
			formData.set('lat', this.state.pointData.coordinates.lat)
			formData.set('lng', this.state.pointData.coordinates.lng)
			formData.set('photo', this.state.pointData.image.content, this.state.pointData.image.name)
			if (this.state.pointData.isChecked) {
				formData.set('size', this.state.pointData.size)
				formData.set('price', this.state.pointData.price)
				formData.set('mark', this.state.pointData.mark)
				formData.set('description', this.state.pointData.description)
				formData.set('checked', 'on')
			}
			try {
				await axios.post('https://shaurma-kh.space/functions/api/add_user_point.php', formData,
					{
						headers: {
							'Content-Type': 'multipart/form-data'
						}
					})
				this.setState({
					isLoading: false
				})
				this.showPopupHandler()
				this.resetFormFields()

			} catch (e) {
				console.log(e)
				alert('Возникла ошибка')
			}
		} else {
			alert('Вы не заплнили обязательные поля ' + errors.join(', '))
		}
		console.log(this.state.pointData)
	}
	b64toBlob = (b64Data, contentType, sliceSize) => {
		contentType = contentType || '';
		sliceSize = sliceSize || 512;
		const byteCharacters = atob(b64Data);
		const byteArrays = [];
		for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
			const slice = byteCharacters.slice(offset, offset + sliceSize);
			const byteNumbers = new Array(slice.length);
			for (let i = 0; i < slice.length; i++) {
				byteNumbers[i] = slice.charCodeAt(i);
			}
			const byteArray = new Uint8Array(byteNumbers);
			byteArrays.push(byteArray);
		}
		const blob = new Blob(byteArrays, { type: contentType });
		return blob;
	}

	render() {
		const fileButtonOpts = {}
		if (!this.state.pointData.image) {
			fileButtonOpts['disabled'] = 'disabled'
		}

		return (
			<div className="app-section add-section">
				<Helmet>
					<title>Добавить точку на карту</title>
					<meta name="description"
						content="" />
				</Helmet>
				<div className="section-inner">
					{this.state.isPopupShow
						? <FormPopup hidePopupHandler={this.hidePopupHandler} />
						: null
					}

					<div className="section-title">
						Добавить место
					</div>

					{this.state.isLoading
						? <Preloader />
						: <form name="addPoint" className="add-form" onSubmit={this.addNewPoint}>
							<div className="add-section-rows">

								<div className="add-section-row">
									<div className="add-section-row-inner">
										<div className="add-section-text">Положение на карте</div>
										<div id="map-add">
											<GoogleMapReact
												bootstrapURLKeys={{ key: 'AIzaSyBwNc1HlSSxYEoDyWFykjCcZqqosUMEfHc' }}
												center={this.state.mapSettings.center}
												zoom={this.state.mapSettings.zoom}
												onClick={this.handleMapClick}
											>
												<OwnPositionPoint
													lat={this.state.mapSettings.center.lat}
													lng={this.state.mapSettings.center.lng}
												/>
												<FormMapPoint
													lat={this.state.pointData.coordinates.lat}
													lng={this.state.pointData.coordinates.lng}
													isChecked={this.state.pointData.isChecked}
												/>
											</GoogleMapReact>
										</div>
									</div>
								</div>
								<div className="add-section-row">
									<div className="add-section-row-inner">
										<div className="add-section-text">Фото</div>
										<div className="add-section-input">
											<label className="upload-file-label">
												{this.state.pointData.previewImage
													? <img src={this.state.pointData.previewImage} alt="" className="img-preview" />
													: <svg width="101" height="101" viewBox="0 0 101 101" fill="none" xmlns="http://www.w3.org/2000/svg" className="img-preview">
														<g clip-path="url(#clip0)">
															<rect width="101" height="101" fill="white" />
															<circle cx="50.5" cy="51.5" r="50.5" fill="white" />
															<rect x="3" y="80.9924" width="94.2046" height="1.63578" fill="#C4C4C4" />
															<path fill-rule="evenodd" clip-rule="evenodd" d="M77.902 58.8276H22.21V59.0652L4.02478 80.3055L4.02479 80.6079L4.02032 80.6132L4.02479 80.6143V80.9106H96.978L77.902 58.8276Z" fill="#1EA463" />
															<path d="M84.2467 66.6127C84.2648 66.6385 84.2797 66.6637 84.2919 66.6881L84.508 66.934L84.3577 66.9704C84.3595 66.9865 84.3611 66.9938 84.3621 66.9969C84.3621 66.9969 84.3586 66.9873 84.3466 66.9731L84.209 67.0065L82.9693 66.6259C82.8224 66.5243 82.64 66.4678 82.429 66.431C82.0586 66.3665 81.5997 66.3623 81.0209 66.3623H76.4353C75.358 66.3623 74.3716 66.1472 73.4911 65.8942C73.0487 65.7671 72.618 65.6258 72.2094 65.4914L72.1803 65.4818C71.7772 65.3492 71.3971 65.2242 71.0237 65.1161L70.9609 64.8814C70.9086 64.7606 70.879 64.6322 70.9338 64.4975C70.9896 64.3603 71.1193 64.2496 71.2737 64.1505C71.2932 64.138 71.3086 64.1259 71.3338 64.1062L71.3338 64.1062C71.3547 64.0899 71.3826 64.0681 71.4248 64.0367C71.4791 63.9962 71.5963 63.909 71.7575 63.8503L71.7668 63.844C71.7875 63.8295 71.8133 63.8077 71.8478 63.7753C71.8645 63.7596 71.8813 63.7433 71.8999 63.725L71.9042 63.7208L71.9043 63.7208C71.9211 63.7044 71.9397 63.6862 71.9584 63.6685C71.997 63.6319 72.0494 63.5841 72.1126 63.5406C72.2269 63.4618 72.3526 63.3956 72.4607 63.3409C72.4964 63.3229 72.529 63.3066 72.5595 63.2915L72.5596 63.2914C72.6298 63.2565 72.6888 63.2271 72.749 63.1938C72.8179 63.1557 72.9106 63.0949 72.9671 63.0504C73.0566 62.9799 73.1671 62.9329 73.2171 62.9117L73.2246 62.9084C73.248 62.8984 73.2603 62.8929 73.2657 62.8904C73.3381 62.8375 73.4233 62.801 73.5046 62.7758C73.5188 62.7714 73.5325 62.7675 73.5453 62.764C73.5851 62.7316 73.6232 62.7078 73.6444 62.6946L73.6496 62.6914L73.6644 62.6819C73.6693 62.6747 73.6762 62.6635 73.6891 62.6425C73.7037 62.6186 73.7263 62.5823 73.7614 62.5416C73.7604 62.5138 73.7668 62.4907 73.7729 62.4748C73.7793 62.4581 73.7872 62.4442 73.7937 62.4343C73.8063 62.415 73.8201 62.3997 73.8286 62.3908C73.8757 62.3414 73.9377 62.3066 73.9911 62.2831C74.0055 62.2698 74.0199 62.2576 74.0336 62.2468C74.0667 62.2205 74.1078 62.1927 74.129 62.1784L74.139 62.1716L74.1513 62.1629C74.1657 62.1159 74.1934 62.0777 74.2167 62.0506C74.2374 62.0264 74.2616 62.0037 74.2748 61.9915L74.2747 61.9822C74.2746 61.975 74.2746 61.9658 74.2747 61.9562C74.2754 61.9189 74.2793 61.8547 74.3218 61.7832C74.3957 61.659 74.4437 61.5219 74.449 61.4004C74.4642 61.05 74.5859 60.6006 74.9851 60.243C75.0376 60.196 75.0818 60.1427 75.1403 60.0692L75.1508 60.0561C75.2033 59.9901 75.2694 59.9071 75.3547 59.8259C75.3827 59.7992 75.4122 59.7707 75.4428 59.7411L75.4428 59.7411L75.4428 59.741C75.5067 59.6792 75.5754 59.6127 75.6453 59.5492L75.6464 59.5469L75.6478 59.5439C75.6508 59.5378 75.6552 59.5284 75.6598 59.5194C75.6703 59.4989 75.6907 59.4613 75.7285 59.4211C75.7599 59.3879 75.8082 59.3474 75.8827 59.31L75.8893 59.304C75.9005 59.2937 75.9123 59.2822 75.9258 59.2689L75.9461 59.249C75.9527 59.2426 75.9609 59.2346 75.9694 59.2268C76.0072 59.1921 76.0541 59.1631 76.0776 59.1488C76.1079 59.1304 76.1419 59.1114 76.175 59.0934C76.2414 59.0574 76.3207 59.017 76.3952 58.9796C76.4363 58.9589 76.4824 58.9359 76.5243 58.9151L76.5243 58.915L76.604 58.8753C76.6653 58.8446 76.6918 58.8306 76.6966 58.828L76.6975 58.8276L77.3626 59.1332L77.6003 59.0755L83.9714 66.3235C84.0777 66.4078 84.1703 66.5038 84.2467 66.6127Z" fill="#226F08" />
															<path d="M41.2405 80.3801C41.3111 80.3777 41.3791 80.3737 41.4441 80.3686L42.1339 80.3695L42.1652 80.2677C42.2056 80.2609 42.2242 80.2582 42.2323 80.2572C42.2323 80.2572 42.2072 80.26 42.1675 80.2602L42.1962 80.167L40.7765 79.6599C40.469 79.6274 40.2589 79.553 40.0861 79.453C39.7831 79.2776 39.5944 79.0232 39.3693 78.6998L37.5864 76.1374C37.1676 75.5354 36.2541 75.0891 35.2883 74.7205C34.8031 74.5353 34.2874 74.3636 33.7974 74.2008L33.7625 74.1892C33.279 74.0286 32.823 73.8772 32.4115 73.7213L31.8087 73.8006C31.4908 73.8303 31.163 73.8764 30.8524 73.9727C30.536 74.0708 30.3135 74.1972 30.1293 74.3318C30.1061 74.3488 30.0823 74.3634 30.0436 74.387C30.0115 74.4067 29.9687 74.4328 29.9076 74.4718C29.8291 74.5218 29.6597 74.6299 29.5779 74.7486C29.575 74.7507 29.5711 74.7534 29.566 74.7568C29.5381 74.7755 29.4945 74.8005 29.4281 74.8356C29.3959 74.8526 29.3621 74.8699 29.3245 74.8892L29.3158 74.8937L29.3157 74.8937C29.2818 74.9111 29.2442 74.9304 29.2078 74.9495C29.1327 74.9889 29.0354 75.0415 28.9527 75.098C28.803 75.2003 28.6886 75.3029 28.5959 75.3899C28.5653 75.4187 28.538 75.4448 28.5125 75.4693L28.5124 75.4693L28.5124 75.4693C28.4536 75.5256 28.4041 75.5729 28.3455 75.6227C28.2785 75.6798 28.1647 75.7613 28.077 75.8145C27.9381 75.8989 27.8652 75.9836 27.8322 76.0219L27.8272 76.0277C27.8117 76.0456 27.8027 76.0552 27.7987 76.0594C27.6964 76.1257 27.6398 76.191 27.6093 76.2488C27.604 76.2589 27.5996 76.2684 27.5959 76.2773C27.5316 76.3153 27.4878 76.3482 27.4635 76.3665L27.4575 76.371L27.4401 76.3839C27.4243 76.3901 27.3993 76.3994 27.3526 76.4169C27.2993 76.4367 27.2185 76.4671 27.132 76.5065C27.063 76.5195 27.0087 76.5343 26.9718 76.5455C26.9331 76.5572 26.9021 76.5684 26.8801 76.5769C26.8375 76.5933 26.8052 76.6085 26.7865 76.6176C26.683 76.668 26.6215 76.7196 26.5844 76.7609C26.5571 76.7755 26.5328 76.7894 26.5113 76.8024C26.4595 76.8337 26.407 76.8702 26.3799 76.889L26.3798 76.889L26.367 76.898L26.3504 76.909C26.2403 76.94 26.1567 76.9741 26.099 77.0003C26.0475 77.0237 26.0009 77.0483 25.976 77.0616L25.953 77.0661C25.9353 77.0696 25.9127 77.074 25.8891 77.0788C25.7973 77.0974 25.6408 77.1308 25.4811 77.1895C25.2039 77.2914 24.8846 77.3851 24.5872 77.4472C23.7296 77.6266 22.6697 77.9137 21.9437 78.3112C21.8482 78.3635 21.734 78.4142 21.5758 78.4827L21.5474 78.495C21.4053 78.5565 21.2264 78.6339 21.0595 78.7212C21.0046 78.7498 20.9458 78.7802 20.8847 78.8117L20.8847 78.8118C20.7571 78.8776 20.62 78.9484 20.4907 79.0185L20.4854 79.0202L20.4787 79.0225C20.4646 79.0271 20.4434 79.0341 20.423 79.0411C20.3765 79.0569 20.2917 79.0867 20.2074 79.1274C20.1377 79.1612 20.0569 79.2079 19.9935 79.2678L19.9814 79.2744C19.9604 79.2857 19.9367 79.2978 19.9092 79.3119L19.868 79.3329C19.8546 79.3397 19.8383 79.3482 19.8223 79.3568C19.7514 79.3948 19.6982 79.4352 19.6722 79.4552C19.6386 79.4812 19.6049 79.5095 19.5736 79.5367C19.5107 79.5914 19.4419 79.6554 19.3786 79.7153C19.3436 79.7484 19.305 79.7853 19.2698 79.8189L19.2698 79.8189L19.2028 79.8828C19.1509 79.932 19.1268 79.9536 19.1223 79.9576L19.1216 79.9583L20.1332 80.1809L20.0836 80.3419L40.4209 80.3673C40.6701 80.3856 40.9425 80.3906 41.2405 80.3801Z" fill="#226F08" />
															<path fill-rule="evenodd" clip-rule="evenodd" d="M33.4703 59.0568C33.5426 59.1933 33.4505 59.3407 33.2437 59.4198C32.7817 59.5965 32.6201 59.9 32.5272 60.3718C32.512 60.4488 32.498 60.5352 32.4833 60.6253C32.458 60.7811 32.4309 60.9482 32.393 61.0975C32.3299 61.3457 32.2252 61.611 31.9992 61.8517C31.1414 62.7654 29.8067 63.4663 28.5709 64.1152C28.52 64.142 28.4692 64.1686 28.4187 64.1952C28.102 64.3617 27.767 64.5106 27.4546 64.6494C27.3672 64.6883 27.2816 64.7263 27.1986 64.7637C26.8085 64.9398 26.4523 65.1124 26.1335 65.3244C25.2317 65.9239 24.0401 66.6038 22.5826 66.9103C22.1292 67.0057 21.6999 67.0144 21.3292 67.0021C21.1589 66.9964 20.9929 66.986 20.8465 66.9767C20.836 66.9761 20.8256 66.9754 20.8154 66.9748C20.6554 66.9647 20.5204 66.9567 20.3936 66.954C19.4108 66.9331 18.4336 66.94 17.4423 66.947C16.9394 66.9506 16.433 66.9542 15.9203 66.9542C15.7256 66.9542 15.5446 66.8966 15.4417 66.8017C15.3389 66.7069 15.3286 66.5884 15.4146 66.4882L21.9765 58.8411L32.9298 58.8276C33.1784 58.8273 33.398 58.9204 33.4703 59.0568Z" fill="#226F08" />
															<path d="M88.5253 74.7764C88.2692 74.6952 87.9439 74.6901 87.7246 74.7639C87.2348 74.9288 86.5732 74.9047 85.59 74.7905C85.4295 74.7718 85.2503 74.7495 85.0633 74.7261C84.7401 74.6858 84.3936 74.6426 84.0799 74.6106C83.5586 74.5573 82.9908 74.5186 82.4383 74.5471C80.3411 74.6553 78.5398 75.0671 76.8718 75.4484L76.8718 75.4484L76.6663 75.4954C76.2385 75.593 75.8412 75.7055 75.4707 75.8104C75.3671 75.8398 75.2655 75.8685 75.166 75.8962C74.6979 76.0263 74.2464 76.1411 73.7257 76.2244C72.2532 76.4601 70.5351 76.8095 69.4986 77.4142C69.1762 77.6023 69.0364 77.808 68.9558 77.9923C68.9188 78.077 67.5265 77.1928 67.5035 77.2671C67.5019 77.2724 64.6695 76.6625 65.8031 77.8717C65.6528 78.1148 66.5158 79.1339 66.3699 79.3831C66.3145 79.4777 66.2069 80.4676 66.3699 80.5496C66.5329 80.6316 68.4095 80.6002 68.6372 80.5923L95.6579 80.5496L91.3097 75.1511C91.3811 75.0304 88.7814 74.8576 88.5253 74.7764Z" fill="#226F08" />
															<path d="M75.0765 58.8278C75.6451 58.8278 75.5557 58.788 74.731 59.9026C74.6434 60.0209 74.5547 60.1295 74.4814 60.2174C74.4453 60.2607 74.4093 60.3032 74.3782 60.3399L74.3764 60.342C74.3434 60.381 74.3156 60.4139 74.2888 60.446C74.2351 60.5104 74.1998 60.5546 74.1743 60.5891C74.1515 60.6199 74.1438 60.6331 74.1438 60.6331C73.9513 60.9806 73.4841 61.571 73.161 61.9681C72.8522 62.3478 72.3799 62.914 72.1006 63.1657C71.9863 63.2687 71.8887 63.3656 71.749 63.5043L71.7488 63.5045C71.7073 63.5458 71.662 63.5907 71.6115 63.6406C71.4134 63.8362 71.1508 64.0905 70.8203 64.3569L70.8193 64.3578L70.8193 64.3578C70.8047 64.3718 70.626 64.5417 70.2813 64.9282C69.964 65.2841 69.5918 65.7252 69.2272 66.177C68.8623 66.6291 68.5177 67.0761 68.2524 67.4435C68.0002 67.7928 67.8911 67.9737 67.8638 68.0191C67.8581 68.0285 67.8559 68.0321 67.8567 68.0301C67.8124 68.1371 67.7534 68.2422 67.6801 68.3444C66.0573 70.6075 64.3971 72.2168 61.9102 73.3027C59.5991 74.3118 56.9134 74.691 54.4794 75.0346L54.0956 75.0889C53.9816 75.1051 53.8663 75.1185 53.7501 75.1291C52.2954 75.2617 50.8508 75.2605 49.6492 75.2403C49.1006 75.2311 48.6376 75.2194 48.2141 75.2088L48.2139 75.2088H48.2138C47.5791 75.1928 47.0329 75.179 46.4195 75.179H39.7805C37.7677 75.179 35.5763 75.3522 33.8544 75.6852C32.8696 75.8757 31.8273 76.0871 30.8877 76.3375C29.891 76.6031 29.3466 76.8265 29.1285 76.9593C28.7901 77.1654 28.3992 77.3653 28.1689 77.4831L28.0696 77.534C27.9404 77.6007 27.8569 77.6454 27.7935 77.6815C27.7461 77.7086 27.7258 77.722 27.7227 77.7241L27.7224 77.7242C27.3079 78.0238 26.8533 78.2374 26.5082 78.3807C26.1695 78.5215 25.8345 78.6352 25.6379 78.7018L25.5312 78.7378L25.531 78.7379C25.376 78.7903 25.3034 78.8148 25.237 78.8395C24.7082 79.1443 24.1786 79.4996 23.5631 79.9125C23.4584 79.9827 22.3075 80.837 22.1975 80.9106H9.68848C10.2296 80.8119 13.0768 78.9372 13.6686 78.7334C13.8217 78.708 16.2283 77.5542 16.2989 77.5419C16.7112 77.47 17.2494 77.2156 17.362 77.1818C17.9891 76.7603 18.7774 76.2304 19.5832 75.7733C19.9886 75.5433 20.4004 75.3744 20.7072 75.2588C20.9588 75.164 21.2258 75.0742 21.3848 75.0207L21.3849 75.0207L21.4499 74.9987C21.5041 74.9804 21.5465 74.9659 21.5816 74.9537C21.965 74.6996 22.358 74.4953 22.604 74.3682L22.7957 74.2694C23.0218 74.153 23.1173 74.1039 23.2067 74.0494C24.4484 73.2933 26.0832 72.7743 27.4012 72.423C28.7764 72.0565 30.1842 71.7765 31.2011 71.5799C33.9493 71.0484 37.069 70.8247 39.7805 70.8247H46.4195C47.1808 70.8247 48.0806 70.8464 48.8615 70.8653L48.8616 70.8653L48.8619 70.8653C49.2446 70.8745 49.5986 70.8831 49.8938 70.888C50.88 70.9046 51.6509 70.8962 52.2873 70.8469C55.0998 70.4461 56.1079 70.2647 56.9442 69.8995C57.7307 69.5561 58.7407 68.8593 60.1865 66.8694C60.357 66.5399 60.6218 66.1581 60.8487 65.8439C61.1624 65.4095 61.5501 64.9077 61.945 64.4184C62.3402 63.9288 62.7553 63.4359 63.1268 63.0192C63.4546 62.6517 63.8662 62.2079 64.2334 61.9119C64.3534 61.8152 64.4742 61.7025 64.648 61.5308L64.7365 61.443L64.7366 61.4428C64.8752 61.305 65.0585 61.1226 65.2536 60.9445L65.2694 60.9274C65.3064 60.8871 65.3589 60.828 65.4253 60.7514C65.5572 60.599 65.7215 60.4029 65.8882 60.1979C66.0553 59.9926 66.2164 59.7886 66.344 59.6196C66.4477 59.4823 66.5002 59.4061 66.519 59.3787L66.519 59.3786C66.5273 59.3666 66.5291 59.3641 66.5258 59.3699C66.6925 59.069 66.5476 58.8278 67.1162 58.8278L75.0765 58.8278Z" fill="#176FFF" />
															<path d="M18.7575 63.6451L58.4346 63.6646L81.1072 63.664" stroke="white" stroke-opacity="0.6" />
															<path d="M13.0894 70.3145H49.9323H86.7753" stroke="white" stroke-opacity="0.6" />
															<path d="M39.7297 59.1299H22.1584L4.02032 80.5923L96.1953 80.5923L77.7063 59.1299H39.7297Z" stroke="white" />
															<line y1="-0.5" x2="23.5786" y2="-0.5" transform="matrix(-0.384631 0.92307 -0.99305 -0.117689 34.6283 58.8276)" stroke="white" stroke-opacity="0.6" />
															<path d="M65.8167 58.8932L68.0839 64.3344L69.2175 67.0549L70.3512 69.7755L74.8857 80.6579" stroke="white" stroke-opacity="0.6" />
															<path d="M49.9324 59.2366L49.9324 80.5923" stroke="white" stroke-opacity="0.6" />
															<path d="M57.3009 68.5008C57.3009 68.8347 53.7481 69.1053 49.3655 69.1053C44.9829 69.1053 41.4301 68.8347 41.4301 68.5008C41.4301 68.1669 44.9829 67.8962 49.3655 67.8962C53.7481 67.8962 57.3009 68.1669 57.3009 68.5008Z" fill="#333333" />
															<path d="M64.1754 29.6326C47.5202 25.6671 40.3822 28.2975 37.6064 29.0906" stroke="black" stroke-linecap="round" stroke-linejoin="round" />
															<path d="M64.5718 29.4828C57.8305 30.6724 56.5345 35.8276 50.9828 36.6207C43.8448 36.6207 43.9512 31.069 37.2098 29.4828C37.2098 22.0364 43.0272 16 50.6925 16C58.3579 16 64.5718 22.0364 64.5718 29.4828Z" fill="#EA4335" />
															<circle cx="50.6926" cy="29.4827" r="4.36207" fill="#A60D0D" />
															<path d="M37.1035 28.6808L37.7153 28.8556C38.0312 28.9459 45.8276 30.2759 50.5862 35.431L64.7183 29.0965L55.7948 64.7904L55.6478 64.8744C53.4887 66.1072 51.5935 66.5832 49.6848 66.5044C47.7886 66.4262 45.9124 65.8013 43.795 64.8945L43.5987 64.8104L37.1035 28.6808Z" fill="#D9AF7F" />
															<path fill-rule="evenodd" clip-rule="evenodd" d="M64.7224 29.0794L55.7948 64.7896L55.6478 64.8735C53.4887 66.1064 51.5935 66.5823 49.6849 66.5036C47.7887 66.4254 45.9124 65.8005 43.7951 64.8937L43.5629 64.7942L43.5548 64.5417C43.1584 52.0544 44.0403 44.3095 47.1813 39.1422C50.3407 33.9444 55.7327 31.4339 64.0749 29.249L64.7224 29.0794Z" fill="#E8BF87" />
														</g>
														<defs>
															<clipPath id="clip0">
																<rect width="101" height="101" fill="white" />
															</clipPath>
														</defs>
													</svg>
												}
												<input type="file" name="image" onChange={this.changeInputFileHandler} />
												<span className="upload-file"></span>
											</label>
											<button className="remove-file"  {...fileButtonOpts} onClick={this.removeLoadedFile}></button>
										</div>
									</div>
									<div className="add-section-row-inner">
										<div className="add-section-text">Название</div>
										<div className="add-section-input">
											<input type="text" name="name" value={this.state.pointData.name} onChange={this.changeInputHandler} />
										</div>
									</div>
									<div className="add-checkbox-wrap">
										<label>
											<span className="checkbox">
												<input type="checkbox" name="checked" className="add-checkbox" onChange={this.changePointStatus} />
												<svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="https://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M14.2334 0L16 1.01984L7.53564 15.6807L7.7085 15.9802L7.18994 16.2795L6.77393 17L6.35791 16.7598L5.94189 17L5.52588 16.2795L5.00732 15.9801L5.18018 15.6807L0 6.70844L1.7666 5.6886L6.35791 13.6409L14.2334 0Z" fill="#5C95F3"></path></svg>
											</span>
											<span className="add-checkbox-text">
												Я попробовал эту шаурму
												</span>
										</label>
									</div>
								</div>

								{this.state.pointData.isChecked
									? <React.Fragment>
										<div className="add-section-row add-section-row-full">
											<div className="add-section-row-inner third">
												<div className="add-section-text">Размер</div>
												<div className="add-section-input">
													<select name="size" onChange={this.changeInputHandler}>
														<option disabled value="">Выбрать</option>
														<option value="Очень маленькая">Очень маленькая</option>
														<option value="Маленькая">Маленькая</option>
														<option value="Средняя">Средняя</option>
														<option value="Большая">Большая</option>
														<option value="Огромная">Огромная</option>
													</select>
												</div>
											</div>
											<div className="add-section-row-inner third">
												<div className="add-section-text">Цена</div>
												<div className="add-section-input">
													<input type="text" name="price" value={this.state.pointData.price} onChange={this.changeInputHandler} />
												</div>
											</div>
											<div className="add-section-row-inner third">
												<div className="add-section-text">Оценка</div>
												<div className="add-section-input">
													<select name="mark" onChange={this.changeInputHandler} >
														<option disabled>Выбрать</option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
														<option value="6">6</option>
														<option value="7">7</option>
														<option value="8">8</option>
														<option value="9">9</option>
														<option value="10">10</option>
													</select>
												</div>
											</div>
											<div className="add-section-row-inner">
												<div className="add-section-text">Описание</div>
												<div className="add-section-input">
													<textarea name="description" value={this.state.pointData.description} onChange={this.changeInputHandler} />
												</div>
											</div>
										</div>
									</React.Fragment>
									: null
								}
							</div>
							<div className="button-wrap">
								<button type="submit" className="add-button">Добавить</button>
							</div>
						</form>
					}
				</div>
			</div>
		)
	}
}

export default Form;