import React from 'react';
import './Map.scss';
import axios from 'axios'
import MapMainSearchbar from '../../components/MapSearchbar/MapSearchbar'

import MapMain from '../../components/MapMain/MapMain'
import { Helmet } from "react-helmet";


class Map extends React.Component {
	state = {
		isLoading: true,
		// showInfoPopup: this.props.showInfoPopup ? true : false,
		activePoint: {
			lat: this.props.pointLat ? this.props.pointLat : null,
			lng: this.props.pointLng ? this.props.pointLng : null,
		},
		ownPosition: {
			lat: null,
			lng: null
		},
		pointsList: {},
		mapSettings: {
			zoom: (this.props.pointLat && this.props.pointLng) ? 19 : 11,
			center: {
				lat: this.props.pointLat ? this.props.pointLat : 50.00000,
				lng: this.props.pointLng ? this.props.pointLng : 36.22917
			}
		}
	}

	updateActivePoint = (lat, lng) => {
		this.setState({
			activePoint: {
				lng,
				lat
			},
			showInfoPopup: true
		})
	}
	getPointsList = async () => {
		try {
			const response = await axios.get('https://shaurma-kh.space/functions/api/get_points_list.php')
			const pointsList = {}
			response.data.forEach(point => {
				pointsList[point.id] = point;
			});
			this.setState({
				pointsList,
				isLoading: false
			})
		} catch (e) {
			console.log(e)
			alert('Возникла ошибка. Невозможно получить список точек. Попробуйте, пожалуста, позже или обратитесь в поддержку.')
		}
	}
	getOwnPosition = () => {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(position => {
				const pos = {
					lat: position.coords.latitude,
					lng: position.coords.longitude
				}
				this.setState({
					ownPosition: pos,
					mapSettings: {
						center: {
							lat:null,
							lng:null
						},
						zoom: 16
					}
				});
				this.setState({
					mapSettings: {
						center: pos,
						zoom: 16
					}
				})
			})
		}
	}
	getNearestPoint = async (lat, lng) => {
		try {
			const response = await axios.get('https://shaurma-kh.space/functions/api/get_nearest.php', {
				params: {
					lat,
					lng
				}
			})
			this.setState({
				mapSettings: {
					center: {
						lat:null,
						lng:null
					},
					zoom: 16
				}
			});
			this.setState({
				mapSettings: {
					center: {
						lat: Number(response.data.lat),
						lng: Number(response.data.lng)
					},
					zoom: 16
				}
			})
		} catch (e) {
			console.log(e)
			alert('Возникла ошибка. Не удается получить данные о ближайшей точке. Попробуйте, пожалуста, позже или обратитесь в поддержку.')
		}

	}
	pointActivityStatusChangeHandler = (id, inactiveStatus) => {
		this.setState({
			pointsList: {
				...this.state.pointsList,
				[id]: {
					...this.state.pointsList[id],
					inactive: inactiveStatus
				}
			}
		})
	}

	// showPopupWithPointInfoHandler = () => (
	// 	this.setState({
	// 		showInfoPopup: true
	// 	})
	// )
	// hidePopupWithPointInfoHandler = () => {
	// 	this.setState({
	// 		showInfoPopup: false
	// 	})
	// }

	componentDidMount() {
		this.getPointsList()
	}

	render() {
		return (
			<div className="app-section map-section">
				<Helmet>
					<title>【Шаурма Харьков】 — Ищем где в Харькове самая вкусная шаурма. Обзоры лучших шаурм, рейтинг и отзывы.</title>
					<meta name="description"
						content="Подробна карта шаурмы Харькова: ты сможешь за пару минут узнать где ближайший ларек с шаурмой в Харькове. Теперь можно быстро узнать златные места для покупки шаурмы." />
				</Helmet>

				<MapMain
					center={{
						lat: this.state.mapSettings.center.lat,
						lng: this.state.mapSettings.center.lng
					}}
					zoom={this.state.mapSettings.zoom}
					// updateActivePoint={this.updateActivePoint}
					ownPosition={this.state.ownPosition}
					pointsList={this.state.pointsList}
				/>

				<MapMainSearchbar
					getOwnPosition={this.getOwnPosition}
					getNearestPoint={this.getNearestPoint}
					ownPosition={this.state.ownPosition}
				/>
				 {this.props.children}
				{/* {this.props.showInfoPopup */}
					{/* ? <MapPopup */}
						{/* lat={this.props.pointLat.lat} */}
						{/* lng={this.props.pointLng.lng} */}
						{/* // onClosePopup={this.props.popupClose} */}
						{/* // onClosePopupLocal={this.hidePopupWithPointInfoHandler} */}
						{/* onActivityStatusChange={this.pointActivityStatusChangeHandler} */}
					{/* /> */}
					{/* : null */}
				{/* } */}

			</div>
		)
	}
}
export default Map