import React from 'react'

import { Switch, Route } from 'react-router-dom'
import { Online, Offline } from 'react-detect-offline'

import './App.scss'

import Nav from './components/Nav/Nav'
import Map from './pages/Map/Map'
import MapPopup from './components/MapPopup/MapPopup'
import Rating from './pages/Rating/Rating'
import Form from './pages/Form/Form'
import Info from './pages/Info/Info'
import Error404 from './components/Error404/Error404'
import OfflineNote from './components/OfflineNote/OfflineNote'


class App extends React.Component {
	state = {
		activepoint: {
			lat: null,
			lng: null
		},

		showInfoPopup: false
	}
	clickPointHandler = (lat, lng) => {
		this.setState({
			activepoint: {
				lat,
				lng
			},
			showInfoPopup: true
		})
	}
	resetActivePointHandler = () => {
		this.setState({
			activepoint: {
				lat: null,
				lng: null,
			},
			showInfoPopup: false
		})
	}
	componentDidMount() {
		if (!localStorage.getItem('userId')) {
			const now = new Date().getTime();
			localStorage.setItem('userId', now);
		}
	}
	render() {
		return (
			<div className="App">
				<Online>
					<React.Fragment>
						<div className="app-wrap">
							<Switch>
								<Route exact path="/">
									<Map/>
								</Route>
								<Route path="/point/:id">
									<Map>
										<MapPopup/>
									</Map>
								</Route>
								<Route path="/rating">
									<Rating
										clickPointHandler={this.clickPointHandler}
									/>
								</Route>
								<Route path="/add-point">
									<Form />
								</Route>
								<Route path="/about">
									<Info />
								</Route>
								<Route path="*" status={404}>
									<Error404 />
								</Route>
							</Switch>
						</div>
						<Nav />
					</React.Fragment>
				</Online>
				<Offline>
					<OfflineNote />

				</Offline>
			</div>
		);
	}
}

export default App;
