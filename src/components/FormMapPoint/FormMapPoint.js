import React from 'react'
import iconNoChecked from './../../img/icon_no-checked.svg'
import iconGuest from './../../img/icon_guest.svg'


const FormMapPoint = props => {
	const iconType = props.isChecked ? iconGuest : iconNoChecked 
	return (
		<div
			style={{
				width: '20px',
				transform: 'translate(-50%,-100%)'
			}}
		>
			<img
				style={{
					width: '100%',
					height: 'auto'
				}}
				src={iconType} alt="" />
		</div>

	)
}



export default FormMapPoint