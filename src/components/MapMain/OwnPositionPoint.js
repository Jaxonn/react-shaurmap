import React from 'react'
import ownPointIcon from './../../img/me_icon.svg'
const OwnPositionPoint = props => {
	
	
	return (
		<div
			style={{
				width: '30px',
				transform: 'translate(-50%,-50%)'
			}}
		>
			<img
				style={{
					width: '100%',
					height: 'auto'
				}}
				src={ownPointIcon} 
				alt=""
			/>
		</div>

	)
}



export default OwnPositionPoint