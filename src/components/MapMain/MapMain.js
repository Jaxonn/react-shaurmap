import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react'
import MapPoint from './MapPoint'
import OwnPositionPoint from './OwnPositionPoint'


class MapMain extends Component {
	state = {
		isLoading: true,
	}
	render() {
		return (
			<div id="map">
				<GoogleMapReact
					bootstrapURLKeys={{ key: 'AIzaSyBwNc1HlSSxYEoDyWFykjCcZqqosUMEfHc' }}
					center={this.props.center}
					zoom={this.props.zoom}
					onChange={console.log('a')}
				>
					{Object.keys(this.props.pointsList).map((point, index) => (
						<MapPoint
							key={index}
							lat={this.props.pointsList[point].lat}
							lng={this.props.pointsList[point].lng}
							id={this.props.pointsList[point].id}
							type={this.props.pointsList[point].checked}
							isInactive={this.props.pointsList[point].inactive}
							// updateActivePoint={this.props.updateActivePoint}
						/>
					))}
					{this.props.ownPosition
						? <OwnPositionPoint
							lat={this.props.ownPosition.lat}
							lng={this.props.ownPosition.lng}
						/>
						: null
					}
				</GoogleMapReact>
			</div>
		);
	}
}


export default MapMain;