import React from 'react';
import { Link } from 'react-router-dom';
import iconDeafault from './../../img/shaurma_icon.svg';
import iconInactive from './../../img/icon_inactive.svg';
import iconNoChecked from './../../img/icon_no-checked.svg';
import iconGuest from './../../img/icon_guest.svg';

const MapPoint = props => {
	let iconType = null
	if (props.isInactive === "0") {
		switch (props.type) {
			case '0':
				iconType = iconNoChecked
				break
			case '1':
				iconType = iconDeafault
				break
			case '2':
				iconType = iconGuest
				break
			default:
				iconType = iconDeafault
		}
	} else {
		iconType = iconInactive
	}
	return (
		<Link
			to={'/point/'+props.id}
			// onClick={() => {props.updateActivePoint(props.lat, props.lng)}}
			style={{
				cursor: 'pointer',
				width: '25px',
				transform: 'translate(-50%,-100%)',
				display: 'block'
			}}
		>
			<img
				style={{
					width: '100%',
					height: 'auto'
				}}
				src={iconType} alt="" />
		</Link>

	)
}



export default MapPoint