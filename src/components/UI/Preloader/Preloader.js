import React from 'react'
import './Preloader.scss'

export const Preloader = () => (
	<div className="preloader"><div/></div>
)