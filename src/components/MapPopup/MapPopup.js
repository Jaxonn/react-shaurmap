import React, { Component } from 'react';
import './MapPopup.scss';
import { Link } from 'react-router-dom';
import { Preloader } from '../UI/Preloader/Preloader';
import ShareButtons from '../ShareButtons/ShareButtons';
import axios from 'axios';
import noImage from './../../img/logo_full.svg';
import { Helmet } from 'react-helmet';
class MapPopup extends Component {
	state = {
		isPhotoZoomed: false,
		userID: null,
		isVisibleReportForm: false,
		isLoading: true,
		typesOfPoint: [
			'Не проверено',
			'Проверено администрацией',
			'Проверено пользователем'
		],
		pointInfo: {
			checked: null,
			description: null,
			id: null,
			img: noImage,
			inactive: null,
			lat: null,
			lng: null,
			mark: null,
			mark_avg: 0,
			mark_qn: 0,
			name: null,
			price: null,
			size: null,
		}
	}
	componentDidMount() {
		const id = window.location.pathname.replace(/[^\d]/g, '');
		console.log(id)
		const userID = localStorage.getItem('userId')
		this.setState({
			userID
		})
		this.loadPointInfo(id, userID);
	}

	loadPointInfo = async (id, userID) => {
		try {
			const response = await axios.get('https://shaurma-kh.space/functions/api/get_point_details.php', {
				params: {
					id,
					userID
				}
			})
			this.setState({
				pointInfo: response.data,
				isLoading: false
			})
		} catch (e) {
			console.log(e)
			alert('Возникла ошибка')
		}
	}
	changePointActivityStatus = async id => {
		try {
			await axios.post('https://shaurma-kh.space/functions/api/change_point_activity.php', {
				id: id
			})
			const inactiveNew = this.state.pointInfo.inactive === '0' ? '1' : '0'
			// this.props.onActivityStatusChange(id, inactiveNew)
			this.setState({
				pointInfo: {
					...this.state.pointInfo,
					inactive: inactiveNew
				}
			})

		} catch (e) {
			console.log(e)
			alert('Возникла ошибка')
		}
	}
	changeVisibilityReportForm = () => {
		this.setState({
			isVisibleReportForm: !this.state.isVisibleReportForm
		})
	}
	sendReportForm = async event => {
		event.preventDefault()
		const reportTextarea = event.target.querySelector('.textarea-report')
		const text = reportTextarea.value
		const formData = {
			point: this.state.pointInfo.name,
			id: this.state.pointInfo.id,
			text: text
		}
		try {
			await axios.post('https://shaurma-kh.space/functions/api/send_report.php', formData)
			alert('Сообщение отправлено')
			reportTextarea.value = ''
			this.setState({
				isVisibleReportForm: false
			})
		} catch (e) {
			console.log(e)
			alert('Произошла ошибка. Не удалось отправить сообщение. Попробуйте, пожалуйста, позже или свяжитесь с администрацией.')
		}

	}
	sendPointRating = async event => {
		const rating = event.target.value

		try {
			await axios.post('https://shaurma-kh.space/functions/api/rating_change.php', {
				id: this.state.pointInfo.id,
				user: this.state.userID,
				mark: rating
			})
			alert('Оценка поставлена.')
			// this.loadPointInfo(this.props.lat, this.props.lng, this.state.userID)
		} catch (e) {
			console.log(e)
			alert('Произошла ошибка. Не удалось поставить оценку.  Попробуйте, пожалуйста, позже или свяжитесь с администрацией.')
		}
	}
	changeZoomStatus = () => {
		this.setState({
			isPhotoZoomed: !this.state.isPhotoZoomed
		})
	}
	render() {
		let title = `【Шаурма Харьков】 — Подробная информация о точке с шаурмой ${this.state.pointInfo.name}.`
		return (
			<div className="map-details-container">
				<Helmet>
					<title>{title}</title>
					<meta name="description"
						content="Подробна карта шаурмы Харькова: ты сможешь за пару минут узнать где ближайший ларек с шаурмой в Харькове. Теперь можно быстро узнать злачные места для покупки шаурмы." />
				</Helmet>
				<Link className="map-details-overlay" to={'/'} />
				<div className="map-details">
					<Link
						to={'/'}
						className="map-details-close"
					// onClick={() => {
					// 	this.props.onClosePopup()
					// 	this.props.onClosePopupLocal()
					// }}
					/>
					<div className="map-details-wrapper">
						{this.state.isLoading
							? <Preloader />
							: <React.Fragment>
								<div className="map-details-title">{this.state.pointInfo.name ? this.state.pointInfo.name : 'Без названия'}</div>
								<div className="map-details-row">
									<div className={`image-overlay ${this.state.isPhotoZoomed ? 'visible' : ''}`} onClick={this.changeZoomStatus}></div>
									<div className="img-container" onClick={this.changeZoomStatus}>
										<img src={this.state.pointInfo.img ? this.state.pointInfo.img : noImage} alt={this.state.pointInfo.name ? this.state.pointInfo.name : 'Без названия'} className={`img-details ${this.state.isPhotoZoomed ? 'scale' : ''}`} />
										<div className="zoom-icon-wrap">
											<svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0" y="0" viewBox="0 0 1000 1000" enableBackground="new 0 0 1000 1000"><path d="M500.3,255.2H377.7v122.6H255.2v122.6h122.6v122.6h122.6V500.3h122.6V377.8H500.3V255.2z M967.9,861.2l-177.1-177c48.5-69.6,77.2-154,77.2-245.2c0-236.9-192.1-429-429-429c-236.9,0-429,192.1-429,429c0,236.9,192.1,429,429,429c91.2,0,175.6-28.7,245.2-77.2l177,177c29.5,29.5,77.2,29.5,106.7,0S997.3,890.7,967.9,861.2z M439,745.5c-169.2,0-306.5-137.2-306.5-306.5c0-169.2,137.2-306.5,306.5-306.5c169.2,0,306.5,137.2,306.5,306.5C745.5,608.3,608.3,745.5,439,745.5z" fill="rgba(0,0,0,.3)"/></svg>
										</div>
									</div>
								</div>
								<div className="map-details-row center">
									<a href={`https://maps.google.com?q=${this.state.pointInfo.lat},${this.state.pointInfo.lng}`} target="_blank" rel="noopener noreferrer" className="google-map-link">Открыть в Google Maps</a>
								</div>
								<div className="map-details-row">
									<div className="map-details-value author">{this.state.typesOfPoint[this.state.pointInfo.checked]}</div>
								</div>
								<div className="map-details-row">
									<div className="map-details-text">Цена:</div>
									<div className="map-details-value price">{this.state.pointInfo.price ? this.state.pointInfo.price : 'Неизвестно'}</div>
								</div>
								<div className="map-details-row">
									<div className="map-details-text">Размер:</div>
									<div className="map-details-value size">{this.state.pointInfo.size ? this.state.pointInfo.size : 'Неизвестно'}</div>
								</div>
								<div className="map-details-row">
									<div className="map-details-text">Оценка:</div>
									<div className="map-details-value rating">{this.state.pointInfo.mark ? this.state.pointInfo.mark : 'Нет'}</div>
								</div>
								<div className="map-details-row">
									<div className="map-details-text">Ваша оценка:</div>
									<div className="map-details-value user-mark">
										<select name="user-mark" onChange={this.sendPointRating}>
											<option value="" disabled>-</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
											<option value="6">6</option>
											<option value="7">7</option>
											<option value="8">8</option>
											<option value="9">9</option>
											<option value="10">10</option>
										</select>
									</div>
								</div>
								<div className="map-details-row">
									<div className="map-details-text">Средняя оценка:</div>
									<div className="map-details-value av-rating">{this.state.pointInfo.mark_avg}</div>
								</div>
								<div className="map-details-row">
									<div className="map-details-text">Количество оценок:</div>
									<div className="map-details-value qn-rating">{this.state.pointInfo.mark_qn}</div>
								</div>
								<div className="map-details-row">
									<div className="map-details-value">Описание:</div>
								</div>
								<div className="map-details-row">
									<div className=" map-details-text description">{this.state.pointInfo.description}</div>
								</div>
								<div className="map-details-row activity-row">
									<button onClick={() => { this.changePointActivityStatus(this.state.pointInfo.id) }} className="activity-button">{this.state.pointInfo.inactive === '0' ? 'Пометить как закрытую' : 'Пометить как открытую'}</button>
								</div>
								<div className="map-details-row report-row">
									<div className="report-row-inner">
										<button onClick={this.changeVisibilityReportForm} className="report-link">Нашли ошибку?</button>
										{this.state.isVisibleReportForm
											? <div className="report-form-wrapper">
												<form onSubmit={this.sendReportForm}>
													<textarea className="textarea-report" placeholder="Напишите, пожалуйста, что оказалось неточно описано?"></textarea>
													<button className="report-button">Отправить сообщение</button>
												</form>
											</div>
											: null

										}
									</div>
								</div>
								<ShareButtons/>
							</React.Fragment>
						}
					</div>
				</div>
			</div>
		)
	}
}

export default MapPopup