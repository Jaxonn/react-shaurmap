import React from 'react'
import './FormPopup.scss'

const FormPopup = props => (
	<div className="success-popup popup">
		<div className="popup-close" onClick={props.hidePopupHandler}></div>
		<div className="success-popup-inner">
			<div className="success-popup-title">
				Спасибо!
			</div>
			<div className="success-popup-text">
				Ваша точка добавлена на&nbsp;карту. Благодарим за&nbsp;информацию.
			</div>
		</div>
	</div>
)

export default FormPopup